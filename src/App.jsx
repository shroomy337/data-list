import { Footer } from "@/components/footer/footer";
import { Header } from "@/components/header/header";
import { Badge } from "@/components/ui/badge";
import {
  DataList,
  DataListItem,
  DataListLabel,
  DataListValue,
} from "@/components/ui/data-list";
import { Separator } from "@/components/ui/separator";

function App() {
  return (
    <div className="h-dvh flex justify-center items-center">
      <Header />
      <div className="flex gap-4">
        <div className="flex flex-col gap-4">
          <h4 className="text-lg font-medium">Vertical</h4>
          <DataList orientation="vertical">
            <DataListItem>
              <DataListLabel>Name</DataListLabel>
              <DataListValue>Artem Ivanov</DataListValue>
            </DataListItem>
            <DataListItem>
              <DataListLabel>Status</DataListLabel>
              <DataListValue className="flex gap-1">
                <Badge>In work</Badge>
                <Badge variant="secondary">Outstuff</Badge>
              </DataListValue>
            </DataListItem>
            <DataListItem>
              <DataListLabel>Position</DataListLabel>
              <DataListValue>Software Engineer</DataListValue>
            </DataListItem>
            <DataListItem>
              <DataListLabel>Email</DataListLabel>
              <DataListValue>
                <a href="mailto:ivanov.artem35@gmail.com">
                  ivanov.artem35@gmail.com
                </a>
              </DataListValue>
            </DataListItem>
          </DataList>
        </div>
        <div className="mx-6">
          <Separator orientation="vertical" />
        </div>
        <div className="flex flex-col gap-4">
          <h4 className="text-lg font-medium">Horizontal</h4>
          <DataList orientation="horizontal">
            <DataListItem>
              <DataListLabel>Name</DataListLabel>
              <DataListValue>Artem Ivanov</DataListValue>
            </DataListItem>
            <DataListItem>
              <DataListLabel>Status</DataListLabel>
              <DataListValue className="flex gap-1">
                <Badge>In work</Badge>
                <Badge variant="secondary">Outstuff</Badge>
              </DataListValue>
            </DataListItem>
            <DataListItem>
              <DataListLabel>Position</DataListLabel>
              <DataListValue>Software Engineer</DataListValue>
            </DataListItem>
            <DataListItem>
              <DataListLabel>Email</DataListLabel>
              <DataListValue>
                <a href="mailto:ivanov.artem35@gmail.com">
                  ivanov.artem35@gmail.com
                </a>
              </DataListValue>
            </DataListItem>
          </DataList>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default App;
