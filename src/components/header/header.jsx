import { Button, buttonVariants } from "@/components/ui/button";

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { dataListCode } from "@/lib/data-list-code";
import { cn } from "@/lib/utils";
import { CheckIcon, ClipboardCopyIcon, CopyIcon } from "@radix-ui/react-icons";
import { LucideGitlab } from "lucide-react";
import React from "react";
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "../ui/tooltip";

export const Header = () => {
  const [hasCopied, setHasCopied] = React.useState(false);

  React.useEffect(() => {
    setTimeout(() => {
      setHasCopied(false);
    }, 2000);
  }, [hasCopied]);

  return (
    <div className="absolute top-4 flex items-center gap-1">
      <TooltipProvider delayDuration={0}>
        <Dialog>
          <Tooltip>
            <TooltipTrigger asChild>
              <DialogTrigger asChild>
                <Button variant="outline">
                  <ClipboardCopyIcon />
                </Button>
              </DialogTrigger>
            </TooltipTrigger>
            <TooltipContent>Copy code</TooltipContent>
          </Tooltip>
          <DialogContent className="max-w-2xl outline-none">
            <DialogHeader>
              <DialogTitle>Edit profile</DialogTitle>
              <DialogDescription>
                Copy and paste the following code into your primitives folder.
              </DialogDescription>
            </DialogHeader>

            <pre className="max-h-[450px] overflow-x-auto rounded-lg border bg-zinc-950 py-4 px-2 dark:bg-zinc-900 relative">
              <Button
                variant="secondary"
                className="sticky left-[90%] top-4 z-10"
                size="sm"
                onClick={() => {
                  navigator.clipboard.writeText(dataListCode);
                  setHasCopied(true);
                }}
              >
                {hasCopied ? (
                  <CheckIcon className="h-4 w-4" />
                ) : (
                  <CopyIcon className="h-4 w-4" />
                )}
              </Button>
              <code className="rounded font-mono text-sm text-muted whitespace-break-spaces">
                {dataListCode}
              </code>
            </pre>
          </DialogContent>
        </Dialog>
      </TooltipProvider>
      <TooltipProvider delayDuration={0}>
        <Tooltip>
          <TooltipTrigger asChild>
            <a
              href="https://gitlab.com/shroomy337/data-list"
              className={cn(
                buttonVariants({ variant: "secondary" }),
                "bg-amber-500 text-muted hover:bg-amber-600"
              )}
              target="_blank"
              rel="noreferrer"
            >
              <LucideGitlab className="h-4 w-4" />
            </a>
          </TooltipTrigger>
          <TooltipContent>GitLab Repository</TooltipContent>
        </Tooltip>
      </TooltipProvider>
    </div>
  );
};
