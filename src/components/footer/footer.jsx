import { cn } from "@/lib/utils";
import { buttonVariants } from "../ui/button";

export const Footer = () => {
  return (
    <div className="text-xs absolute bottom-4 font-mono">
      developed by{" "}
      <a
        href="https://twitter.com/shroomyeu"
        target="_blank"
        rel="noopener noreferrer"
        className={cn(buttonVariants({ variant: "link" }), "p-0 text-xs")}
      >
        mykyta
      </a>
    </div>
  );
};
