export const dataListCode = `import { cn } from "@/lib/utils";
import React, { createContext, useContext } from "react";

const DataListContext = createContext();

const useDataList = () => {
  const context = useContext(DataListContext);
  if (!context) {
    throw new Error("DataListItem must be used within a DataList.");
  }
  return context;
};

const DataList = React.forwardRef(
  ({ orientation = "horizontal", ...props }, ref) => {
    const { className, ...rest } = props;

    return (
      <DataListContext.Provider value={{ orientation }}>
        <dl
          ref={ref}
          {...rest}
          className={cn(
            "gap-3 text-start",
            orientation === "horizontal" && "grid grid-cols-1",
            orientation === "vertical" && "flex flex-col",
            className
          )}
        />
      </DataListContext.Provider>
    );
  }
);

DataList.displayName = "DataList";

const DataListItem = React.forwardRef((props, ref) => {
  const { className, ...rest } = props;
  const { orientation } = useDataList();

  return (
    <div
      ref={ref}
      {...rest}
      className={cn(
        "gap-1 text-sm",
        orientation == "horizontal" &&
          "grid grid-cols-subgrid items-baseline col-span-2",
        orientation == "vertical" && "flex flex-col",
        className
      )}
    />
  );
});

DataListItem.displayName = "DataListItem";

const DataListLabel = React.forwardRef((props, ref) => {
  const { className, ...rest } = props;
  const { orientation } = useDataList();

  return (
    <dt
      ref={ref}
      {...rest}
      className={cn(
        "text-muted-foreground truncate",
        orientation === "horizontal" && "min-w-[120px]",
        orientation === "vertical" && "min-w-0",
        className
      )}
    />
  );
});

DataListLabel.displayName = "DataListLabel";

const DataListValue = React.forwardRef((props, ref) => {
  const { className, ...rest } = props;

  return (
    <dd
      ref={ref}
      {...rest}
      className={cn(
        "flex min-w-0 m-0 mt-0 mb-0 first-of-type:mt-0 last-of-type:mb-0",
        className
      )}
    />
  );
});

DataListValue.displayName = "DataListValue";

export { DataList, DataListItem, DataListLabel, DataListValue };
`;
